﻿using Plugin.Media;
using Scandit.BarcodePicker.Unified;
using Scandit.BarcodePicker.Unified.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ScanditFreeze
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(true)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            // must be set before you use the picker for the first time. use a license key that matches the bundle identifier in info.plist!
            ScanditService.ScanditLicense.AppKey = "-- ENTER YOUR SCANDIT LICENSE KEY HERE --";
            // 
        }

        private async void BtnCamera_Pressed(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();

            using (var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                SaveToAlbum = false,
                CompressionQuality = 90,
                ModalPresentationStyle = Plugin.Media.Abstractions.MediaPickerModalPresentationStyle.OverFullScreen,
            }))
            {
                if (file == null)
                    return;

            }
        }
        private async void BtnScan_Pressed(object sender, EventArgs e)
        {
            var settings = ScanditService.BarcodePicker.GetDefaultScanSettings();

            settings.CameraPositionPreference = CameraPosition.Back;

            // Enable symbologies that you want to scan.
            settings.EnableSymbology(Symbology.Code128, true);
            settings.EnableSymbology(Symbology.DataMatrix, true);

            ScanditService.BarcodePicker.AlwaysShowModally = true;

            ScanditService.BarcodePicker.DidStop += BarcodePicker_DidStop;
            ScanditService.BarcodePicker.DidScan += BarcodePicker_DidScan;

            await ScanditService.BarcodePicker.ApplySettingsAsync(settings);

            // Start the scanning process.
            await ScanditService.BarcodePicker.StartScanningAsync();

        }

        private void BarcodePicker_DidStop(DidStopReason reason)
        {
            if (reason == DidStopReason.CancelledInUserInterface)
            {
                ///
            }
        }

        private void BarcodePicker_DidScan(ScanSession session)
        {
            var firstCode = session.NewlyRecognizedCodes.First();

                    // firstCode.Data --> do something with it

            ScanditService.BarcodePicker.DidScan -= BarcodePicker_DidScan;

            session.StopScanning();
        }
    }
}
